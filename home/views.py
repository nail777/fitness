import logging

from django.conf import settings
from django.core.mail import send_mail
from django.http import JsonResponse
from django.shortcuts import render
from django.views import View
from telegram import Bot

from home.forms import AdviseFreeForm

bot = Bot('2016300934:AAF8oxTZH4boWTTzL_rH-BxGBmzI_jAWlTU')


class index(View):
    def get(self, request):
        return render(request, "home/index.html", {})


class AdviseFreeView(View):
    """Заявки"""

    def post(self, request):
        response_data = {}
        form = AdviseFreeForm(request.POST)
        if form.is_valid():
            form.save()
            response_data['status'] = True
            response_data['message'] = 'Спасибо'
            mes = f'Имя: {request.POST["name"]}\nТелефон: {request.POST["phone"]}\nПочта: {request.POST["email"]}\n' \
                  f'Продукт: {request.POST["type_product"]}\n '
            try:
                send_mail('Обращение', mes, settings.EMAIL_HOST_USER,
                          [
                              'ksenia2807@bk.ru',
                              # 'nail.velichko2016@yandex.ru'
                          ])
            except Exception as e:
                logging.error(e)
            try:
                bot.send_message(chat_id=400006717, text=mes, parse_mode="Markdown")
            except Exception as e:
                logging.warning(e)
        else:
            response_data['status'] = False
            response_data['message'] = 'Ошибка'
            response_data['form'] = form._errors
        return JsonResponse(response_data, safe=False)
