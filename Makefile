run-http:
	cd front && python -m http.server 8000
run-mig:
	python manage.py makemigrations && python manage.py migrate
copy-nginx:
	ln -s ./fitness.conf /etc/nginx/sites-enabled/
#su postgres -c psql postgres
#root@ovz2:/etc/nginx# systemctl restart nginx
#root@ovz2:/etc/nginx# systemctl status nginx.service
